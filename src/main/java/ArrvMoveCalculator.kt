class ArrvMoveCalculator : MoveCalculatorBase() {
    override fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double {
        //all vehicles with not 0 attack damage are dangerous for arrvs
        val dangerScore = getDangerScore(group, x, y) * 100

        return modifyByAllyCollisions((-dangerScore + getFacilityScore(group, x, y, 30000)), x, y, group, considerEnemy = true)
    }
}


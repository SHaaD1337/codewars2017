import model.VehicleType

import java.util.EnumMap

object VehicleMovePriority {
    private val vehicleMovePriorities = EnumMap<VehicleType, Int>(VehicleType::class.java)

    init {
        vehicleMovePriorities.put(VehicleType.ARRV, 4)
        vehicleMovePriorities.put(VehicleType.TANK, 3)
        vehicleMovePriorities.put(VehicleType.IFV, 2)
        vehicleMovePriorities.put(VehicleType.FIGHTER, 1)
        vehicleMovePriorities.put(VehicleType.HELICOPTER, 1)
    }

    fun priority(type: VehicleType): Int {
        return vehicleMovePriorities[type]!!
    }
}

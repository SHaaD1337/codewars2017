import model.Vehicle
import model.VehicleType
import java.util.*

class Group(val id: Int, val vehicleType: VehicleType) {
    private val units = EnumMap<VehicleType, MutableCollection<Long>>(VehicleType::class.java)

    fun addUnit(vehicle: Vehicle) {
        (units as MutableMap<VehicleType, MutableCollection<Long>>)
                .computeIfAbsent(vehicle.type)
                { mutableSetOf() }.add(vehicle.id)
    }

    fun isSelected(): Boolean {
        val selectedVehicles = Vehicles.myVehicles()
                .filter { it.isSelected }
                .map { it.id }
                .toList()
        return !selectedVehicles.isEmpty() && unitsIds().toSet().containsAll(selectedVehicles)
    }

    fun isInGroup(vehicle: Vehicle): Boolean {
        return units.entries.any { it.value.contains(vehicle.id) }
    }

    fun unitsIds(): Sequence<Long> {
        return units.flatMap { it.value }.asSequence()
    }

    fun vehicles(): Sequence<Vehicle> {
        return unitsIds().map { Vehicles.vehicle(it) }
    }

    fun removeDeadUnit(vehicleId: Long) {
        units.values.forEach { it.removeIf { it == vehicleId } }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val group = other as Group?

        return id == group!!.id && units == group.units
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + units.hashCode()
        return result
    }
}

import model.VehicleType

class TankMoveCalculator : MoveCalculatorBase() {
    override fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double {
        //first step - count target units score
        val targetScore = max(getTargetScores(x, y, VehicleType.IFV, 300),
                getTargetScores(x, y, VehicleType.TANK, 200),
                getTargetScores(x, y, VehicleType.HELICOPTER, 50),
                getTargetScores(x, y, VehicleType.ARRV, 100),
                getFacilityScore(group, x, y))

        //second step - count units which counter current unit
        val counterUnitsScore = getDangerScore(group, x, y)

        return modifyByAllyCollisions(targetScore - counterUnitsScore, x, y, group)
    }
}


import NukeEscapeHandler.escapeFromNuke
import ProductionHandler.setupProduction
import model.*

class MyStrategy : Strategy {
    private val visualizer = Visualizer()
    private val moveCalculator = MoveCalculatorCommon()
    private val nukeCalculator = NukeCoordinatesCalculator()
    private val moveManager = MovementManager(moveCalculator)

    override fun move(me: Player, world: World, game: Game, move: Move) {
        val startTime = System.currentTimeMillis()
        State.initializeStrategy(world, game)
        State.initializeTick(me, world, game, move)
        if (world.tickIndex == 0) {
            VehicleType.values()
                    .sortedBy { r -> VehicleMovePriority.priority(r) }
                    .forEach { this.assignGroupsForVehicleType(it) }
        }
        visualizer.init()

        Moves.checkForConditionMoves()
        RegroupManager.decreaseTicks()
        escapeFromNuke()

        setupProduction()
        bindNewUnits()

        if (me.remainingActionCooldownTicks > 0) {
            return
        }
        if (Moves.queuedMovesCount() > 20) {
            println("too many moves!")
            Moves.executeNextMove()
            return
        }

        moveManager.moveAllGroups()

        if (world.tickIndex % 29 == 0 && world.myPlayer.remainingNuclearStrikeCooldownTicks == 0) {
            Moves.add { m ->
                val nukeCoordinate = nukeCalculator.getNukeCoordinate()
                if (nukeCoordinate != null) {
                    println("LAUNCHING MOTHERFUCKING NUCLEAR MISSILE")
                    m.action = ActionType.TACTICAL_NUCLEAR_STRIKE
                    m.x = nukeCoordinate.second.x
                    m.y = nukeCoordinate.second.y
                    m.vehicleId = nukeCoordinate.first.id
                }
            }
        }

        Moves.executeNextMove()
        val timeSpent = System.currentTimeMillis() - startTime
        if (timeSpent > 2) {
            println(timeSpent)
        }
        visualizer.drawTick(moveCalculator)
    }

    private val facilitiesBeingBinding = mutableSetOf<Long>()
    private fun bindNewUnits() {
        val bindedUnits = GroupManager.idToGroup.values.asSequence().flatMap { it.unitsIds() }.toSet()
        Facilities
                .mineFacilities()
                .filter { !facilitiesBeingBinding.contains(it.id) }
                .filter { it.type == FacilityType.VEHICLE_FACTORY }
                .flatMap { facility ->
                    val createdVehicles = Vehicles.myVehicles()
                            .filter { !bindedUnits.contains(it.id) }
                            .filter {
                                it.x in (facility.left - 10..facility.left + State.game.facilityWidth + 10) &&
                                        it.y in (facility.top - 10..facility.top + State.game.facilityHeight + 10)
                            }.toList()
                    createdVehicles.groupBy { it.type }.values.asSequence()
                            .map { facility to it }
                }.filter { it.second.size > 40 }
                .forEach {
                    println("binding new units")

                    facilitiesBeingBinding.add(it.first.id)
                    val vehicleType = it.second[0].type
                    val box = Box(it.first.left - 2, it.first.top - 2,
                            it.first.left + State.game.facilityWidth + 2, it.first.top + State.game.facilityHeight + 2)
                    assignGroupsForVehicleType(vehicleType, box, it.first)
                    Moves.addMoveToPoint(box, box.middleX, box.minY, vehicleType.speed())
                }
    }

    private fun assignGroupsForVehicleType(vehicleType: VehicleType, bb: Box? = null, facility: Facility? = null) {
        val box = bb ?: Vehicles.myVehiclesBox(vehicleType)

        val groupId = GroupManager.getNextGroupId()
        val group = Group(groupId, vehicleType)
        GroupManager.addGroup(group)
        Vehicles.myVehicles().filter { box.isInside(it) }.forEach { group.addUnit(it) }

        Moves.addBoxSelection(box, vehicleType, {
            Vehicles.myVehicles().filter { box.isInside(it) }.forEach { group.addUnit(it) }
            facility?.let { facilitiesBeingBinding.remove(facility.id) }
            RegroupManager.addRemainingTicks(groupId, 100)
        })
        Moves.addAssign(groupId)
        Moves.addScale(box.middleX, box.middleY, 0.1)
    }

}

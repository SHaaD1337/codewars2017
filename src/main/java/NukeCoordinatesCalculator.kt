import model.Vehicle

class NukeCoordinatesCalculator {
    fun getNukeCoordinate(): Pair<Vehicle, PFieldNode>? {
        val bestPotentialNukePoint = Vehicles.myVehicles()
                .map { vehicle ->
                    getGrid(vehicle)
                            .map { node ->
                                val enemyVehicles = VehicleQuadTree
                                        .query(node.first, node.second, State.game.tacticalNuclearStrikeRadius, Ownership.ENEMY)
                                        .map { getNuclearScore(it.first, it.second) }
                                        .sum()
                                if (enemyVehicles <= 5.0) {
                                    PFieldNode(node.first, node.second, 0.0)
                                }
                                val allyVehicles = VehicleQuadTree
                                        .query(node.first, node.second, State.game.tacticalNuclearStrikeRadius, Ownership.MY)
                                        .map { getNuclearScore(it.first, it.second) }
                                        .sum()
                                PFieldNode(node.first, node.second, (enemyVehicles - allyVehicles))
                            }.maxBy { it.score }!!
                }.maxBy { it.score }
        if (bestPotentialNukePoint == null || bestPotentialNukePoint.score <= 5.0) {
            return null
        }

        val bestVehicleForNuke = VehicleQuadTree
                .query(bestPotentialNukePoint.x, bestPotentialNukePoint.y, 120.0, Ownership.MY)
                .filter { it.second < it.first.visionRange * 0.7 }
                .maxBy { it.second } ?: return null

        return bestVehicleForNuke.first to bestPotentialNukePoint
    }

    private fun getNuclearDamage(distanceToCenter: Double): Double {
        val interval = (1..State.game.tacticalNuclearStrikeRadius.toInt()).firstOrNull { distanceToCenter < it } ?: -1
        return when (interval) {
            -1 -> 0.0
            else -> (interval / State.game.maxTacticalNuclearStrikeDamage) * State.game.maxTacticalNuclearStrikeDamage
        }
    }

    private fun getNuclearScore(vehicle: Vehicle, distance: Double): Double {
        val nuclearDamage = getNuclearDamage(distance)
        val hpAfterHit = vehicle.durability - nuclearDamage
        if (hpAfterHit < 0) {
            return 1.0
        }
        return hpAfterHit / vehicle.maxDurability
    }

    private fun getGrid(vehicle: Vehicle): Sequence<Pair<Double, Double>> {
        val gridNodes = mutableListOf<Pair<Double, Double>>()
        val visionRange = vehicle.visionRange * 0.6
        var x = vehicle.x - visionRange
        while (x <= State.game.worldWidth && x <= vehicle.x + visionRange) {
            var y = vehicle.y - visionRange
            while (y <= State.game.worldHeight && y <= vehicle.y + visionRange) {
                gridNodes.add(x to y)
                y += Constants.NUKE_GRID_STEP
            }
            x += Constants.NUKE_GRID_STEP
        }
        return gridNodes.asSequence()
    }
}

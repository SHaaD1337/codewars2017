object GroupManager {
    val idToGroup = mutableMapOf<Int, Group>()

    fun addGroup(group: Group) {
        idToGroup.put(group.id, group)
    }

    fun removeDeadVehicle(vehicleId: Long) {
        idToGroup.values.forEach { it.removeDeadUnit(vehicleId) }
    }

    fun getNextGroupId(): Int {
        val minExistingEmptyGroup = idToGroup
                .values
                .filter { it.unitsIds().toList().isEmpty() }
                .minBy { it.id }?.id
        if (minExistingEmptyGroup == null) {
            val maxExistingGroupId = idToGroup
                    .values
                    .maxBy { it.id }?.id ?: 0
            return maxExistingGroupId + 1
        }
        return minExistingEmptyGroup
    }

    fun getActiveGroupCount(): Int {
        val count = idToGroup.values.filter { !it.unitsIds().toList().isEmpty() }.count()
        return when (count) {
            0 -> 1
            else -> count
        }
    }

    fun getMoveFrequency() =
            when {
                getActiveGroupCount() <= 5 -> Constants.MOVE_FREQUENCY
                getActiveGroupCount() <= 10 -> Constants.MOVE_FREQUENCY * 4
                getActiveGroupCount() <= 15 -> Constants.MOVE_FREQUENCY * 8
                else -> Constants.MOVE_FREQUENCY * 16
            }
}

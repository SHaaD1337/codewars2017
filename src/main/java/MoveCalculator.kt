import model.VehicleType

interface MoveCalculator {
    fun calculateFieldForUnits(vehicleType: VehicleType, box: Box, group: Group): PFieldNode

    fun getLastField(vehicleType: VehicleType, groupId: Int): List<PFieldNode>?
}

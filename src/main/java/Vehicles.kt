import model.FacilityType
import model.Vehicle
import model.VehicleType

object Vehicles {
    val vehicleById: MutableMap<Long, Vehicle> = mutableMapOf()

    private val vehicles = mutableMapOf<Ownership, MutableMap<VehicleType, MutableCollection<Vehicle>>>()

    fun vehicles(ownership: Ownership = Ownership.ANY, vehicleType: VehicleType? = null): Sequence<Vehicle> {
        val sequence: Sequence<Map<VehicleType, Collection<Vehicle>>> =
                if (ownership == Ownership.ANY) {
                    vehicles.values.asSequence()
                } else {
                    sequenceOf(vehicles[ownership]!!)
                }

        var entrySequence = sequence.flatMap { it -> it.entries.asSequence() }

        if (vehicleType != null) {
            entrySequence = entrySequence.filter { entry -> entry.key === vehicleType }
        }

        return entrySequence.flatMap { it.value.asSequence() }
    }

    fun vehicle(id: Long): Vehicle {
        return vehicleById[id]!!
    }

    fun myVehicles(): Sequence<Vehicle> {
        return vehicles(Ownership.MY)
    }

    fun myVehicles(group: Group): Sequence<Vehicle> {
        return group.unitsIds().map { vehicleById[it]!! }.asSequence()
    }

    fun myVehicles(vehicleType: VehicleType): Sequence<Vehicle> {
        return vehicles(Ownership.MY, vehicleType)
    }

    fun enemyVehicles(): Sequence<Vehicle> {
        return vehicles(Ownership.ENEMY)
    }

    fun enemyVehicles(vehicleType: VehicleType): Sequence<Vehicle> {
        return vehicles(Ownership.ENEMY, vehicleType)
    }

    fun myVehiclesBox(): Box {
        return Coordinates.box(myVehicles().toList())
    }

    fun myVehiclesBox(vehicleType: VehicleType): Box {
        return Coordinates.box(myVehicles(vehicleType).toList())
    }

    fun updateVehicles() {
        vehicles.clear()

        Ownership.values()
                .filter { it != Ownership.ANY }
                .forEach { ownership ->
                    val vehiclesByOwnership = vehicles.computeIfAbsent(ownership) { mutableMapOf() }
                    VehicleType.values()
                            .map { it to mutableListOf<Vehicle>() }
                            .forEach { vehiclesByOwnership.put(it.first, it.second) }
                }

        vehicleById
                .values
                .forEach { v ->
                    val ownership = if (v.playerId == State.me.id) Ownership.MY else Ownership.ENEMY
                    val vehiclesByType = (vehicles).computeIfAbsent(ownership) { mutableMapOf() }
                    val vehicles = vehiclesByType.computeIfAbsent(v.type) { mutableListOf() }
                    vehicles.add(v)
                }
    }

    fun getNextTwoVehiclesInProduction(): Sequence<Point> {
        return Facilities.mineFacilities()
                .filter { it.type == FacilityType.VEHICLE_FACTORY }
                .flatMap { f ->
                    val resultPoints = mutableListOf<Point>()
                    var y = f.top.toInt() + 2
                    while (y <= f.top + State.game.facilityHeight - 2) {
                        var x = f.left.toInt() + 2
                        while (x <= f.left + State.game.facilityWidth - 2) {
                            if (VehicleQuadTree.query(x.toDouble(), y.toDouble(), 6.0, Ownership.MY)
                                    .any { it.second < 4.0 }) {
                                x += 6
                                continue
                            }
                            resultPoints.add(Point(x.toDouble(), y.toDouble()))
                            x += 6
                            if (resultPoints.size >= 2) {
                                break
                            }
                        }
                        y += 6
                        if (resultPoints.size >= 2) {
                            break
                        }
                    }
                    resultPoints.asSequence()
                }

    }
}

enum class Ownership {
    ANY,

    MY,

    ENEMY
}

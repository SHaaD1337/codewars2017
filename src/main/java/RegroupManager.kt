object RegroupManager {
    private val groupId2RegroupTicks: MutableMap<Int, Int> = HashMap()

    fun getRemainingRegroupTicks(groupId: Int): Int {
        return groupId2RegroupTicks.computeIfAbsent(groupId, { 0 })
    }

    fun decreaseTicks() {
        groupId2RegroupTicks.forEach { vehicleType, integer -> groupId2RegroupTicks.put(vehicleType, if (integer <= 0) 0 else integer - 1) }
    }

    fun addRemainingTicks(groupId: Int, ticks: Int): Int {
        return groupId2RegroupTicks.put(groupId, getRemainingRegroupTicks(groupId) + ticks)!!
    }
}

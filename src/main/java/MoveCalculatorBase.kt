import Coordinates.distance
import model.Facility
import model.FacilityType
import model.Vehicle
import model.VehicleType
import model.VehicleType.*
import java.awt.Polygon

abstract class MoveCalculatorBase : MoveCalculator {
    private val group2nodes = mutableMapOf<Int, MutableList<PFieldNode>>()

    override fun calculateFieldForUnits(vehicleType: VehicleType, box: Box, group: Group): PFieldNode {
        val nodes = group2nodes.computeIfAbsent(group.id) { mutableListOf() }
        nodes.clear()

        val maxCountRange = vehicleType.speed() * GroupManager.getMoveFrequency() * 4
        val actualGridStep = maxCountRange / if (group.vehicleType.isAerial()) 1.5 else 2.75
        var x = box.middleX - maxCountRange * 1.5
        while (x <= box.middleX + maxCountRange * 1.5) {
            var y = box.middleY - maxCountRange * 1.5
            while (y <= box.middleY + maxCountRange * 1.5) {
                y += actualGridStep
                if (distance(x, y, box.middleX, box.middleY) > maxCountRange) {
                    continue
                }
                var pointScore = computeScoreForCoordinates(box, group, x, y)// - getMapBorderPenalty(x, y)
                if (State.world.opponentPlayer.nextNuclearStrikeTickIndex != -1) {
                    pointScore -= getNuclearPenalty(x, y)
                }
                if (pointScore == Double.NaN) {
                    println()
                }
                nodes.add(PFieldNode(x, y, pointScore))
            }
            x += actualGridStep
        }

        val node = nodes.maxBy { it.score } ?: nodes[0]

        return if (node.score == 0.0 && !nodes.any { it.score != 0.0 })
            PFieldNode(box.middleX, box.middleY, 0.0)
        else
            node
    }

    private fun getMapBorderPenalty(x: Double, y: Double): Double {
        val left = x - Constants.GRID_STEP
        val moreLeft = left - Constants.GRID_STEP.toDouble() - Constants.GRID_STEP.toDouble()
        val right = x + Constants.GRID_STEP
        val moreRight = x + Constants.GRID_STEP.toDouble() + Constants.GRID_STEP.toDouble()
        val top = y - Constants.GRID_STEP
        val moreTop = y - Constants.GRID_STEP.toDouble() - Constants.GRID_STEP.toDouble()
        val bottom = y + Constants.GRID_STEP
        val moreBottom = y + Constants.GRID_STEP.toDouble() + Constants.GRID_STEP.toDouble()

        var penalty = 0.0

        var enemiesAroundPoint = Vehicles.enemyVehicles()
                .filter { distance(it, x, y) < Constants.GRID_STEP }.count()
        if (enemiesAroundPoint == 0) enemiesAroundPoint = 1

        val maxPenalty = Constants.MAX_MAP_BORDER_PENALTY * 1.0 / enemiesAroundPoint
        val minPenalty = Constants.MIN_MAP_BORDER_PENALTY * 1.0 / enemiesAroundPoint
        when {
            left <= 0 -> penalty += maxPenalty
            moreLeft <= 0 -> penalty += minPenalty
        }

        when {
            top <= 0 -> penalty += maxPenalty
            moreTop <= 0 -> penalty += minPenalty
        }

        when {
            right >= State.world.width -> penalty += maxPenalty
            moreRight >= State.world.width -> penalty += minPenalty
        }

        when {
            bottom >= State.world.height -> penalty += maxPenalty
            moreBottom >= State.world.height -> penalty += minPenalty
        }

        return penalty
    }


    override fun getLastField(vehicleType: VehicleType, groupId: Int): List<PFieldNode>? {
        return group2nodes[groupId]
    }

    protected abstract fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double

    protected fun modifyByAllyCollisions(score: Double,
                                         x: Double,
                                         y: Double,
                                         group: Group,
                                         isAerial: Boolean = false,
                                         considerEnemy: Boolean = false): Double {
        val box = Coordinates.box(Vehicles.myVehicles(group).toList())

        val hulled = ConvexHull.makeHull(Vehicles.myVehicles(group).map { Point(it.x, it.y) }.toList()).toMutableList()

        val maxDifX = x - box.middleX
        val maxDifY = y - box.middleY

        val iterationCount = 20
        val resultModifier = (0..iterationCount).map { i ->
            val currentDifX = maxDifX / iterationCount * i
            val currentDifY = maxDifY / iterationCount * i
            val polygon = Polygon(
                    hulled.map { (it.x + currentDifX).toInt() }.toIntArray(),
                    hulled.map { (it.y + currentDifY).toInt() }.toIntArray(),
                    hulled.size
            )
            val resultOwnership = if (considerEnemy) Ownership.ANY else Ownership.MY
            val vehiclesNearby = VehicleQuadTree.query(box.middleX + currentDifX, box.middleY + currentDifY, 100.0, resultOwnership)
                    .filter { it.first.isAerial == isAerial }
                    .filter { !group.isInGroup(it.first) }
                    .map { Point(it.first.x, it.first.y) }
                    .plus(Vehicles.getNextTwoVehiclesInProduction())
                    .toList()

            if (vehiclesNearby.any { polygon.contains(it.x, it.y) }) return 0.1

            val polygon1 = Polygon(
                    hulled.map { ((if (it.x < box.middleX) it.x - 2 else it.x + 2) + currentDifX).toInt() }.toIntArray(),
                    hulled.map { ((if (it.y < box.middleY) it.y - 2 else it.y + 2) + currentDifY).toInt() }.toIntArray(),
                    hulled.size
            )
            if (vehiclesNearby.any { polygon1.contains(it.x, it.y) }) return 0.3
            1.0
        }.min() ?: 1.0

        val a = when {
            resultModifier != 1.0 && score >= 0 -> -1000000 / resultModifier
            score < 0 -> score / resultModifier * naiveCollisionMultiplier(x, y, group, isAerial)
            else -> score * naiveCollisionMultiplier(x, y, group, isAerial)
        }
        return a
    }

    private fun naiveCollisionMultiplier(x: Double,
                                         y: Double,
                                         group: Group,
                                         isAerial: Boolean = false): Double {

        var filteredVehicles = Vehicles.myVehicles()
                .filter { v -> v.isAerial == isAerial }
                .filter { !group.isInGroup(it) }


        fun getCollisionScore(distanceToPoint: Double): Double {
            val collisionInterval = (1..10).firstOrNull { distanceToPoint < 5 * it } ?: -1
            return when (collisionInterval) {
                -1 -> 1.0
                else -> 1 - 0.01 * collisionInterval
            }
        }

        val resultModifier = filteredVehicles
                .map { v -> getCollisionScore(safeDistanceTo(x, y, v)) }
                .fold(1.0) { l, r -> l * r }

        return resultModifier
    }


    protected fun shouldRepair(group: Group): Boolean {
        val averageHpPercent =
                group.unitsIds().map { Vehicles.vehicle(it) }
                        .map { v -> v.durability * 1.0 / v.maxDurability }
                        .average()

        val arrvCount = Vehicles.myVehicles(VehicleType.ARRV).count()
        return averageHpPercent <= 0.5 && arrvCount >= 5
    }

    protected fun safeDistanceTo(x: Double, y: Double, v: Vehicle): Double {
        val distanceTo = Coordinates.distance(v, x, y)
        return if (distanceTo < 1) 1.0 else distanceTo
    }

    protected fun getTargetScores(x: Double, y: Double, type: VehicleType, reward: Int): Double {
        return Vehicles.enemyVehicles(type)
                .map { v -> rewardByDistance(safeDistanceTo(x, y, v), reward) }.sum()
    }

    protected fun rewardByDistance(distance: Double, reward: Int) = reward * 1.0 / (1..1024).first { distance < Constants.GRID_STEP * it }

    protected fun max(vararg values: Double): Double {
        return values.max() ?: 0.0
    }

    protected fun min(vararg values: Double): Double {
        return values.min() ?: 0.0
    }

    protected fun getNuclearPenalty(x: Double, y: Double): Double {
        val opponentPlayer = State.world.opponentPlayer
        val nextNuclearStrikeX = opponentPlayer.nextNuclearStrikeX
        val nextNuclearStrikeY = opponentPlayer.nextNuclearStrikeY
        val nuclearRadius = State.game.tacticalNuclearStrikeRadius

        val distanceToCenter = distance(x, y, nextNuclearStrikeX, nextNuclearStrikeY)

        return when {
            distanceToCenter < nuclearRadius / 5 -> 100000.0
            distanceToCenter < nuclearRadius / 4 -> 75000.0
            distanceToCenter < nuclearRadius / 3 -> 50000.0
            distanceToCenter < nuclearRadius / 2 -> 25000.0
            distanceToCenter < nuclearRadius -> 10000.0
            distanceToCenter < nuclearRadius / 0.75 -> 5000.0
            else -> 0.0
        }
    }

    protected fun getFacilityScore(group: Group, x: Double, y: Double, reward: Int = 30000): Double {
        fun unitsAroundMultiplier(f: Facility): Double {
            val count = Vehicles.myVehicles()
                    .filter { !it.isAerial }
                    .filter { !group.isInGroup(it) }
                    .filter {
                        it.x in (f.left - 20..f.left + State.game.facilityWidth + 20) &&
                                it.y in (f.top - 20..f.top + State.game.facilityHeight + 20)
                    }.count()
            return count * 1.0 / 100
        }
        return Facilities
                .notMineFacilities()
                .map { facility ->
                    val facilityCenterX = facility.left + State.game.facilityWidth / 2
                    val facilityCenterY = facility.top + State.game.facilityHeight / 2
                    val resultReward = if (facility.type == FacilityType.CONTROL_CENTER) (reward * 0.8).toInt() else reward
                    val rewardByDistance = rewardByDistance(distance(facilityCenterX, facilityCenterY, x, y), resultReward)
                    rewardByDistance - rewardByDistance * unitsAroundMultiplier(facility)
                }.max() ?: 0.0
    }

    private val counterUnitMap = mapOf(
            IFV to listOf(HELICOPTER, FIGHTER, IFV),
            HELICOPTER to listOf(HELICOPTER, TANK),
            FIGHTER to listOf(HELICOPTER, FIGHTER),
            TANK to listOf(TANK, IFV),
            ARRV to listOf())

    protected fun getDangerScore(group: Group, x: Double, y: Double): Double {
        val box = Coordinates.box(group)
        val difX = x - box.middleX
        val difY = y - box.middleY

        val unitsInGroupAfterMove = group.unitsIds()
                .map { Vehicles.vehicle(it) }
                .filter { it.type == group.vehicleType }
                .map { it.x + difX to it.y + difY }
                .filter { distance(it.first, it.second, x, y) < 15 }
                .count()

        val enemyUnitsInPointCount = VehicleQuadTree
                .query(x, y, 15.0)
                .count()

        var unitsCountMultiplier = unitsInGroupAfterMove * 1.0 / enemyUnitsInPointCount
        if (unitsCountMultiplier <= 0.1) {
            unitsCountMultiplier = 0.1
        }
        return group.unitsIds()
                .map { Vehicles.vehicle(it) }
                .map { allyUnit ->
                    val newX = allyUnit.x + difX
                    val newY = allyUnit.y + difY
                    val attack: (Vehicle) -> Int = { if (allyUnit.isAerial) it.aerialDamage else it.groundDamage }
                    val range: (Vehicle) -> Double = { if (allyUnit.isAerial) it.aerialAttackRange else it.groundAttackRange }

                    VehicleQuadTree
                            .query(newX, newY, 100.0, Ownership.ENEMY)
                            .filter { it.second < range(it.first) * 5 }
                            .map { enemyToDistance ->
                                val enemy = enemyToDistance.first
                                val defence = if (enemy.isAerial) allyUnit.aerialDefence else allyUnit.groundDefence
                                var hpMultiplier = enemy.durability * 1.0 / enemy.maxDurability
                                if (hpMultiplier < 0.1) {
                                    hpMultiplier = 0.1
                                }
                                var dangerScore = (attack(enemy) - defence) * hpMultiplier
                                if (counterUnitMap[group.vehicleType]!!.contains(enemy.type)) {
                                    dangerScore /= unitsCountMultiplier
                                }
                                if (dangerScore <= 0) dangerScore = 0.0
                                val dangerInterval = (1..5).firstOrNull { enemyToDistance.second < enemy.aerialAttackRange / 2.5 * it } ?: -1
                                when (dangerInterval) {
                                    -1 -> 0.0
                                    else -> dangerScore / dangerInterval
                                }
                            }
                            .sum()
                }.sum()
    }
}

import model.FacilityType
import model.VehicleType

object ProductionHandler {
    fun setupProduction() {
        if (State.world.tickIndex % 50 != 0) {
            return
        }
        if (Facilities
                .mineFacilities()
                .filter { it.type == FacilityType.VEHICLE_FACTORY }
                .count() == 0) {
            return
        }
        val shouldBuildAntiAir =
                Vehicles.enemyVehicles(VehicleType.FIGHTER).plus(Vehicles.enemyVehicles(VehicleType.HELICOPTER)).count() >
                        Vehicles.enemyVehicles(VehicleType.TANK).plus(Vehicles.enemyVehicles(VehicleType.IFV)).count()

        val requiredVehicleType = if (shouldBuildAntiAir) VehicleType.FIGHTER else VehicleType.TANK

        Facilities
                .mineFacilities()
                .filter { it.type == FacilityType.VEHICLE_FACTORY }
                .filter { it.vehicleType != requiredVehicleType }
                .forEach { facility ->
                    println("setuping production")
                    Moves.addVehicleProduction(requiredVehicleType, facility.id)
                }
    }

}
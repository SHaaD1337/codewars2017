import model.*
import java.awt.Color
import java.util.*

class Visualizer {
    private val rc = RewindClient()
    private var isInitialized = false

    internal fun init() {
        if (isInitialized) {
            return
        }
        drawTerrain()
        isInitialized = true
        rc.endFrame()
    }

    internal fun drawTick(moveCalculator: MoveCalculator) {
        drawVehicles()
        drawMove()
        drawFacilities()
        drawNuclear()
        drawGroupsConvexHull()
        drawNewUnits()

        drawPFieldGrid(VehicleType.FIGHTER, 1, moveCalculator)
        drawPFieldGrid(VehicleType.HELICOPTER, 2, moveCalculator)
        drawPFieldGrid(VehicleType.TANK, 4, moveCalculator)
        drawPFieldGrid(VehicleType.IFV, 5, moveCalculator)


        rc.message("Current tick " + State.world.tickIndex +
                "; Actions in queue " + Moves.queuedMovesCount() +
                "; Mine points: " + State.world.myPlayer.score +
                "; Opponent points: " + State.world.opponentPlayer.score)
        rc.endFrame()
    }

    private fun drawNewUnits() {
        Vehicles.getNextTwoVehiclesInProduction()
                .forEach {
                    rc.circle(it.x, it.y, 1.0, Color.cyan, 5)
                }
    }

    private fun drawGroupsConvexHull() {
        GroupManager.idToGroup
                .forEach { _, g ->
                    if (g.unitsIds().toList().size < 2) {
                        return@forEach
                    }
                    val box = Coordinates.box(Vehicles.myVehicles(g).toList())

                    val hulled = ConvexHull.makeHull(Vehicles.myVehicles(g).map { Point(it.x, it.y) }.toList()).toMutableList()
                    val hulled1 = ConvexHull.makeHull(Vehicles.myVehicles(g).map {
                        Point(if (it.x < box.middleX) it.x - 10 else it.x + 10, if (it.y < box.middleY) it.y - 10 else it.y + 10)
                    }.toList()).toMutableList()
                    val hulled2 = ConvexHull.makeHull(Vehicles.myVehicles(g).map {
                        Point(if (it.x < box.middleX) it.x - 20 else it.x + 20, if (it.y < box.middleY) it.y - 20 else it.y + 20)
                    }.toList()).toMutableList()

                    hulled.add(hulled[0])
                    hulled1.add(hulled1[0])
                    hulled2.add(hulled2[0])

                    (1 until hulled.size).forEach { i ->
                        rc.line(hulled[i - 1].x, hulled[i - 1].y, hulled[i].x, hulled[i].y, Color.ORANGE, 5)
                    }

                    (1 until hulled1.size).forEach { i ->
                        rc.line(hulled1[i - 1].x, hulled1[i - 1].y, hulled1[i].x, hulled1[i].y, Color.ORANGE, 5)
                    }

                    (1 until hulled2.size).forEach { i ->
                        rc.line(hulled2[i - 1].x, hulled2[i - 1].y, hulled2[i].x, hulled2[i].y, Color.ORANGE, 5)
                    }
                }
    }

    private fun drawNuclear() {
        fun drawNuclearByPlayer(player: Player, color: Color) {
            if (player.nextNuclearStrikeTickIndex != -1) {
                rc.circle(player.nextNuclearStrikeX, player.nextNuclearStrikeY, State.game!!.tacticalNuclearStrikeRadius, color, 5)
            }
        }
        drawNuclearByPlayer(State.me, Color(0, 0, 255, 30))
        drawNuclearByPlayer(State.world.opponentPlayer, Color(255, 0, 0, 30))

    }

    private fun drawFacilities() {
        State.world.facilities.forEach {
            rc.facility(it.left.toInt() / 32, it.top.toInt() / 32,
                    when (it.type) {
                        FacilityType.CONTROL_CENTER -> RewindClient.FacilityType.CONTROL_CENTER
                        FacilityType.VEHICLE_FACTORY -> RewindClient.FacilityType.VEHICLE_FACTORY
                    },
                    getSide(it.ownerPlayerId),
                    it.productionProgress,
                    it.vehicleType?.productionCost() ?: 0,
                    it.capturePoints.toInt(),
                    when {
                        it.capturePoints > 0 -> State.game!!.maxFacilityCapturePoints.toInt()
                        else -> -State.game!!.maxFacilityCapturePoints.toInt()
                    })
        }
    }

    private fun drawPFieldGrid(type: VehicleType, layer: Int, moveCalculator: MoveCalculator) {
        GroupManager.idToGroup.values
                .filter { it.vehicleType == type }
                .forEach { group ->
                    drawPFieldGrid(moveCalculator.getLastField(group.vehicleType, group.id), layer)
                }
    }

    private fun drawPFieldGrid(field: Collection<PFieldNode>?, layer: Int) {
        if (State.world.tickIndex < 100) {
            return
        }
        if (field == null || field.isEmpty()) {
            return
        }
        val maxScore = field.maxBy { it.score }!!.score
        val minScore = field.minBy { it.score }!!.score
        val delta = Math.abs((maxScore - minScore) / 10)

        class NodeColorInterval(val min: Double, val max: Double, val color: Color)

        val intervals = ArrayList<NodeColorInterval>()
        intervals.add(NodeColorInterval(minScore - delta * 100, minScore + delta, Color.decode("#FF0000")))
        intervals.add(NodeColorInterval(minScore + delta, minScore + delta * 2, Color.decode("#FF3300")))
        intervals.add(NodeColorInterval(minScore + delta * 2, minScore + delta * 3, Color.decode("#ff6600")))
        intervals.add(NodeColorInterval(minScore + delta * 3, minScore + delta * 4, Color.decode("#ff9900")))
        intervals.add(NodeColorInterval(minScore + delta * 4, minScore + delta * 5, Color.decode("#FFFF00")))
        intervals.add(NodeColorInterval(minScore + delta * 5, minScore + delta * 6, Color.decode("#ccff00")))
        intervals.add(NodeColorInterval(minScore + delta * 6, minScore + delta * 7, Color.decode("#99ff00")))
        intervals.add(NodeColorInterval(minScore + delta * 7, minScore + delta * 8, Color.decode("#66ff00")))
        intervals.add(NodeColorInterval(minScore + delta * 8, minScore + delta * 9, Color.decode("#33ff00")))
        intervals.add(NodeColorInterval(minScore + delta * 9, minScore + delta * 100, Color.decode("#00FF00")))

        field.forEach { x ->
            var found: NodeColorInterval? = intervals.firstOrNull { it.min <= x.score && x.score <= it.max }
            if (found == null) {
//                println("not found for ${x.score}, x: ${x.x}, y:${x.y}")
//                println("min " + minScore)
//                println("max " + maxScore)
                found = intervals[0]
            }
            val color = found.color
            rc.circle(x.x, x.y, 3.0, color, layer)
            rc.popup(x.x, x.y, 3.0, String.format("x:%d;y:%d;score=%f", x.x.toInt(), x.y.toInt(), x.score))
        }
    }

    private fun drawTerrain() {
        for (i in State.terrainTypeByCellXY.indices)
            (0 until State.terrainTypeByCellXY[i].size)
                    .filter { State.terrainTypeByCellXY[i][it] !== TerrainType.PLAIN }
                    .forEach { rc.areaDescription(i, it, getAreaType(State.terrainTypeByCellXY[i][it])) }
        for (i in State.weatherTypeByCellXY.indices)
            (0 until State.weatherTypeByCellXY[i].size)
                    .filter { State.weatherTypeByCellXY[i][it] !== WeatherType.CLEAR }
                    .forEach { rc.areaDescription(i, it, getAreaType(State.weatherTypeByCellXY[i][it])) }
    }

    private fun drawMove() {
        val move = State.move

        //todo: draw all cases
        if (move.action === ActionType.NONE) {
            return
        }
        when {
            move.action === ActionType.CLEAR_AND_SELECT ||
                    move.action === ActionType.ASSIGN ||
                    move.action === ActionType.ADD_TO_SELECTION ||
                    move.action === ActionType.DESELECT -> {
                rc.rect(move.left, move.top, move.right, move.bottom, Color.BLUE, 2)
                return
            }
            move.action === ActionType.MOVE -> {
                val middleX = centerXOfSelectedUnits()
                val middleY = centerYOfSelectedUnits()
                if (!middleX.isNaN() && !middleY.isNaN()) {
                    rc.circle(move.x + middleX, move.y + middleY, 10.0, Color.cyan, 2)
                }
            }
        }
        if (move.action === ActionType.SCALE || move.action === ActionType.ROTATE) {
            rc.circle(move.x, move.y, 5.0,
                    if (move.action === ActionType.SCALE) Color.pink else Color.MAGENTA, 2)
        }
    }

    private fun drawVehicles() {
        //        Vehicles.enemyVehicles(VehicleType.FIGHTER).forEach(x -> {
        //            rc.circle(x.getX(), x.getY(), x.getSquaredAerialAttackRange(), Color.RED, 1);
        //        });
        Vehicles.vehicles().forEach { x ->
            //            rc.circle(x.getX(), x.getY(), x.getVisionRange(), Color.ORANGE);
            rc.livingUnit(x.x, x.y, x.radius, x.durability, x.maxDurability, getSide(x.playerId), 0.0, getVehicleType(x), x.remainingAttackCooldownTicks, x.attackCooldownTicks, x.isSelected)
        }
    }

    private fun getSide(playerId: Long): RewindClient.Side {
        return when (playerId) {
            State.me.id -> RewindClient.Side.OUR
            State.world.opponentPlayer.id -> RewindClient.Side.ENEMY
            else -> RewindClient.Side.NEUTRAL
        }
    }

    private fun getVehicleType(x: Vehicle): RewindClient.UnitType {
        return when {
            x.type == VehicleType.TANK -> RewindClient.UnitType.TANK
            x.type == VehicleType.IFV -> RewindClient.UnitType.IFV
            x.type == VehicleType.ARRV -> RewindClient.UnitType.ARRV
            x.type == VehicleType.HELICOPTER -> RewindClient.UnitType.HELICOPTER
            x.type == VehicleType.FIGHTER -> RewindClient.UnitType.FIGHTER
            else -> throw RuntimeException("Unknown vehicle type")
        }
    }

    private fun getAreaType(x: TerrainType): RewindClient.AreaType {
        return when (x) {
            TerrainType.FOREST -> RewindClient.AreaType.FOREST
            TerrainType.PLAIN -> RewindClient.AreaType.UNKNOWN
            TerrainType.SWAMP -> RewindClient.AreaType.SWAMP
        }
    }

    private fun getAreaType(x: WeatherType): RewindClient.AreaType {
        return when (x) {
            WeatherType.CLEAR -> RewindClient.AreaType.UNKNOWN
            WeatherType.CLOUD -> RewindClient.AreaType.CLOUD
            WeatherType.RAIN -> RewindClient.AreaType.RAIN
        }
    }

    private fun centerXOfSelectedUnits(): Double =
            Vehicles.myVehicles().filter { it.isSelected }
                    .map { it.x }.average()

    private fun centerYOfSelectedUnits(): Double =
            Vehicles.myVehicles().filter { it.isSelected }
                    .map { it.y }.average()
}

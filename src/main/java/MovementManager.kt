import model.ActionType
import model.VehicleType

class MovementManager(private val moveCalculator: MoveCalculator) {

    fun moveAllGroups() {
        GroupManager.idToGroup
                .forEach { _, group ->
                    if (group.vehicleType == VehicleType.HELICOPTER && State.world!!.tickIndex < 200) {
                        return@forEach
                    }
                    if (State.world.tickIndex % GroupManager.getMoveFrequency() == 0) {
                        moveUnits(group)
                    }
                }
    }

    private var group2MoveType = mutableMapOf<Int, Int>()
    private fun moveUnits(group: Group) {
        if (RegroupManager.getRemainingRegroupTicks(group.id) > 0) {
            println("${group.vehicleType} is still regrouping")
            return
        }
        val selectedUnits = Vehicles.myVehicles(group).toList()

        if (selectedUnits.isEmpty()) {
            return
        }

        val unitsBox = Coordinates.box(selectedUnits)

        group2MoveType.computeIfAbsent(group.id, { 1 })
        val moveType = group2MoveType.put(group.id, group2MoveType[group.id]!! + 1)!!

        Moves.addGroupSelection(group.id)
        if (moveType % 10 == 0) {
            Moves.add { m ->
                val box = Coordinates.box(Vehicles.myVehicles(group).toList())
                m.action = ActionType.SCALE
                m.x = box.middleX
                m.y = box.middleY
                m.factor = 0.1
            }
        } else {
            val node = moveCalculator.calculateFieldForUnits(group.vehicleType, unitsBox, group)

            if (node.x == unitsBox.middleX && node.y == unitsBox.middleY) {
                return
            }

            Moves.addMoveToPoint(unitsBox, node.x, node.y, getMaxSpeed(group, unitsBox, node.x, node.y))
        }
    }

    private fun getMaxSpeed(group: Group, box: Box, x: Double, y: Double): Double {
        val maxIterationCount = 20

        val diffX = (x - box.middleX) / maxIterationCount
        val diffY = (y - box.middleY) / maxIterationCount
        return group.vehicleType.speed() * (0..maxIterationCount).map { i ->
            Vehicles.myVehicles(group).map { v ->
                val newX = v.x + diffX * i
                val newY = v.y + diffY * i

                listOf(getCellSpeedFactor(newX - 2, newY - 2, v.isAerial),
                        getCellSpeedFactor(newX - 2, newY + 2, v.isAerial),
                        getCellSpeedFactor(newX + 2, newY + 2, v.isAerial),
                        getCellSpeedFactor(newX + 2, newY - 2, v.isAerial)).min()!!
            }.min()!!
        }.min()!!
    }

    private fun getCellSpeedFactor(x: Double, y: Double, isAerial: Boolean) =
            if (isAerial) {
                State.weatherTypeByCellXY!![correctCellNumber((x / 32).toInt())][correctCellNumber((y / 32).toInt())].speedReduceMultiplier()
            } else {
                State.terrainTypeByCellXY!![correctCellNumber((x / 32).toInt())][correctCellNumber((y / 32).toInt())].speedReduceMultiplier()
            }

    private fun correctCellNumber(cellId: Int) =
            when {
                cellId < 0 -> 0
                cellId > 31 -> 31
                else -> cellId
            }
}

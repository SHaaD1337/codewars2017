import model.VehicleType

object CounterUnitsHolder {
    //todo probably remove this
    private val type2CounterUnitsList: Map<VehicleType, Map<VehicleType, Int>> =
            mapOf(
                    VehicleType.FIGHTER to mapOf(
                            VehicleType.IFV to 20,
                            VehicleType.FIGHTER to 2),
                    VehicleType.HELICOPTER to mapOf(
                            VehicleType.FIGHTER to 100,
                            VehicleType.HELICOPTER to 15,
                            VehicleType.TANK to 250,
                            VehicleType.IFV to 15),
                    VehicleType.TANK to mapOf(
                            VehicleType.HELICOPTER to 10,
                            VehicleType.TANK to 5),
                    VehicleType.IFV to mapOf(
                            VehicleType.IFV to 2,
                            VehicleType.TANK to 10,
                            VehicleType.HELICOPTER to 2),
                    VehicleType.ARRV to mapOf(
                            VehicleType.IFV to 10,
                            VehicleType.HELICOPTER to 10,
                            VehicleType.TANK to 10)
            )

    fun getCounterScore(type: VehicleType, counterType: VehicleType): Int {
        val units2Score = type2CounterUnitsList[type] ?: return 0
        return units2Score[counterType] ?: return 0
    }

    fun getCounterTypes(type: VehicleType): Collection<VehicleType> {
        val units2Score = type2CounterUnitsList[type] ?: return listOf()
        return units2Score.keys
    }
}
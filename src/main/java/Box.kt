import model.CircularUnit

class Box(val minX: Double, val minY: Double, val maxX: Double, val maxY: Double) {
    constructor(minX: Int, minY: Int, maxX: Int, maxY: Int) :
            this(minX.toDouble(), minY.toDouble(), maxX.toDouble(), maxY.toDouble())

    fun hasNan(): Boolean {
        return maxX.isNaN() || maxY.isNaN() || minX.isNaN() || minY.isNaN()
    }

    val middleX: Double
        get() = (minX + maxX) / 2

    val middleY: Double
        get() = (minY + maxY) / 2

    val area: Double
        get() {
            if (this.hasNan()) {
                return 0.0
            }
            return (maxX - minX) * (maxY - minY)
        }

    fun isInside(unit: CircularUnit): Boolean {
        return unit.x in minX..maxX && unit.y in minY..maxY
    }
}

mkdir version-$(date +"%m_%d_%Y-%H:%M")
cp -r /home/shaad/Projects/java-cgdk/src/main/java/* version-$(date +"%m_%d_%Y-%H:%M")
rm -r version-$(date +"%m_%d_%Y-%H:%M")/model
rm version-$(date +"%m_%d_%Y-%H:%M")/RemoteProcessClient.java
rm version-$(date +"%m_%d_%Y-%H:%M")/Strategy.kt
rm version-$(date +"%m_%d_%Y-%H:%M")/Runner.java
rm version-$(date +"%m_%d_%Y-%H:%M")/RewindClient.java
rm version-$(date +"%m_%d_%Y-%H:%M")/Visualizer.kt
cat version-$(date +"%m_%d_%Y-%H:%M")/MyStrategy.kt | grep -v visualizer > version-$(date +"%m_%d_%Y-%H:%M")/MyStrategy1.kt
mv -f version-$(date +"%m_%d_%Y-%H:%M")/MyStrategy1.kt version-$(date +"%m_%d_%Y-%H:%M")/MyStrategy.kt
zip -rj version-$(date +"%m_%d_%Y-%H:%M").zip version-$(date +"%m_%d_%Y-%H:%M")/*
